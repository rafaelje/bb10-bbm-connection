// Default empty project template
#include "applicationui.hpp"
#include "bbm/RegistrationHandler.hpp"
#include "bbm/Profile.hpp"

#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/AbstractPane>

using namespace bb::cascades;

ApplicationUI::ApplicationUI(bb::cascades::Application *app)
: QObject(app)
{

    // Every application is required to have its own unique UUID. You should
    // keep using the same UUID when you release a new version of your
    // application.
    // TODO:  YOU MUST CHANGE THIS UUID!
    // You can generate one here: http://www.guidgenerator.com/
    const QUuid uuid(QLatin1String("39864b29-3851-4cf6-bcd9-203794d91be7"));

//! [0]
    // Register with BBM.
    RegistrationHandler *registrationHandler = new RegistrationHandler(uuid, this);
    //RegistrationHandler registrationHandler(uuid, this);
//! [0]

    Profile *profile = new Profile(registrationHandler->context(), this);

    QObject::connect(registrationHandler, SIGNAL(registered()), profile, SLOT(show()));

    // create scene document from main.qml asset
    // set parent to created document to ensure it exists for the whole application lifetime
    QmlDocument *qml = QmlDocument::create("asset:///main.qml").parent(this);

    qml->setContextProperty("_registrationBBM", registrationHandler);
    qml->setContextProperty("_profile", profile);


    // create root object for the UI
    AbstractPane *root = qml->createRootObject<AbstractPane>();
    // set created root object as a scene
    app->setScene(root);
}

