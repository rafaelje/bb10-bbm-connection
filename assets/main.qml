// Default empty project template
import bb.cascades 1.0

// creates one page with a label
Page {
    Container {
        layout: DockLayout {}
        Button {
            text: qsTr("BBM")
            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Center
            
            onClicked: {

                _registrationBBM.registerApplication();
            }
        }
        
    }
}

